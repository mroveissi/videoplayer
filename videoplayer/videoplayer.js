(function( $ ) {

  $.fn.mrplayer = function(options ) {
    var opts = $.extend( {}, $.fn.mrplayer.defaults, options );
    var mrovplObjID='';

    mrovplObjID=$(this).attr('id');

    var outputHtml=htmlOutputGenerate(mrovplObjID);

    $(this).replaceWith(outputHtml)
    var obj=$("#"+mrovplObjID);
    var mrovplHandelDrag=0;
    var mrovplVHandelDrag=0;
    var handleDragStart,handleDragEnd;
    var handleVDragStart,handleVDragEnd;
    var mrovplIsPLaying=0;
    var mrovplMediaPlayer;
    var activeSrc="";
    var buffCanvas = document.getElementById('mrovplBufferCanvas_'+mrovplObjID);
    var buffContext = buffCanvas.getContext('2d');


    buffContext.fillStyle = '#4f4f4f';
    buffContext.strokeStyle = 'white';
    var buffInc;



    mrovplMediaPlayer = document.getElementById("mrovpl_video_"+mrovplObjID);
    mrovplMediaPlayer.controls = false;

    generateSources(obj);
    createUi(obj);
    addEvents(obj);
    this.changeSource=function(sources){
      opts.sources=sources;
      generateSources(obj);
      mrovplMediaPlayer.pause();
      mrovplMediaPlayer.currentTime=0;
      $(".mrovpl-quality[data-source="+activeSrc+"]").addClass('active');
      $(".mrovpl-quality.active").trigger('click')



    };
    return this;
    /*return this.each(function(){
      //IF MAMY OBJECT
    });*/
    function generateSources(obj){
      obj.find('video').children('source').remove();
      obj.find('.morvpl-quality-changer').children('.mrovpl-quality').remove();
      for(var i in opts.sources){
        video=opts.sources[i];
        if(video.default=="true" || opts.sources.length==1){

          activeSrc="mrovplSRC_"+mrovplObjID+"_"+i;
          activeButton=i;
          obj.find(".mrovpl-btn-quality").text(video.label)
        }else{
          activeButton=-1;
        }
        buttonStr='<button class="mrovpl-quality '+(activeButton>0 ? 'active' : '' )+'" data-label="'+video.label+'" data-source="mrovplSRC_'+mrovplObjID+'_'+i+'">'+video.label+'</button>';
        obj.find('.morvpl-quality-changer').append(buttonStr);
        videoStr='<source id="mrovplSRC_'+mrovplObjID+'_'+i+'" src="'+video.file+'" >';
        obj.find('video').append(videoStr);
      }
      if(opts.sources.length==1){
        obj.find(".mrovpl-btn-quality").hide();
      }else{
        obj.find(".mrovpl-btn-quality").show();
      }
    }
    function test(){
      alert(mrovplObjID);
    }
    function createUi(obj){

      obj.css({
        width:opts.width,
        height:opts.height
      });

      if(opts.image!=""){

        obj.find(".mrovpl-image").css("backgroundImage","url('"+opts.image+"')")
      }
    }
    function htmlOutputGenerate(id){
        var output=''+
'<div id="'+id+'" class="mrovpl-video-player">'+
    '<div class="mrovpl-image">'+
      '<button class="mrovpl-play-icon mrovpl-btn-play">'+
        '<div class="mrovpl-st-icon">' +
          '<i class="icon-play"></i>' +
        '</div>'+
      '</button>'+
    '</div>'+
    '<div class="mrovpl-buffering">'+
      '<i class="icon-spinner5"></i>'+
    '</div>'+
    '<div class="mrovpl-main-vid-container">'+
      '<video class="mrovpl-main-video "  id="mrovpl_video_'+id+'" >'+
      'Your browser does not support HTML5 video.'+
      '</video>'+
    '</div>'+
    '<div class="mrovpl-control-bar" >'+
      '<div class="mrovpl-proggress-bar" >'+
        '<span class="mrovpl-current-time"></span>'+
        '<span class="mrovpl-total-time"></span>'+
        '<div class="mropl-proggress-buffer">'+
          '<canvas class="mrovplBufferCanvas" id="mrovplBufferCanvas_'+id+'"></canvas>'+
        '</div>'+
        '<div class="mropl-proggress"></div>'+
        '<div class="mropl-proggress-handel"></div>'+
      '</div>'+
      '<div class="mrovpl-buttons-group mrovpl-left-buttons">'+
        '<button class="mrovpl-button mrovpl-btn-backward"><i class="icon-backward2"></i></button>'+
        '<button class="mrovpl-button mrovpl-btn-refresh"><i class="icon-loop2"></i></button>'+
        '<button class="mrovpl-button mrovpl-btn-play"><i class="icon-play"></i></button>'+
      '</div>'+
      '<div class="mrovpl-buttons-group mrovpl-right-buttons">'+
        '<div  class="mrovpl-button-frame">'+
          '<div class="mrovpl-volume-changer">'+
            '<div class="mrovpl-v-progress-bar">'+
              '<div class="mrovpl-v-progress"></div>'+
              '<div class="mrovpl-v-progress-handel"></div>'+
            '</div>'+
          '</div>'+
          '<button class="mrovpl-button mrovpl-btn-volume"><i id="mrovplVolIcon_'+id+'" class="icon-volume-high"></i></button>'+
        '</div>'+
        '<button class="mrovpl-button mrovpl-btn-fullscreen"><i class="icon-enlarge"></i></button>'+
        '<div  class="mrovpl-button-frame">'+
          '<div class="morvpl-quality-changer">'+
          '</div>'+
          '<button class="mrovpl-button mrovpl-text-mode mrovpl-btn-quality"></button>'+
        '</div>'+
      '</div>'+
    '</div>'+
'</div>';
      return output;
    }
    function addEvents(obj){

      addEvents_main(obj);
      addEvents_mainProgress(obj);
      addEvents_volumeProgress(obj);
      addEvents_buttons(obj);
      addEvents_playButtons(obj);
      addEvents_video(obj);
    }
    function addEvents_main(obj){
      obj.find('*').click(function(e){
        if ( $(e.target).closest('.morvpl-quality-changer,.mrovpl-btn-quality').length === 0 ) {
          obj.find(".morvpl-quality-changer").hide();
          obj.find(".mrovpl-btn-quality").removeClass("active");
        }
        if ( $(e.target).closest('.mrovpl-volume-changer,.mrovpl-btn-volume').length === 0 ) {
          obj.find(".mrovpl-volume-changer").hide();
          obj.find(".mrovpl-btn-volume").removeClass("active");

        }
      });
      obj.mouseup(function(e){
        mrovplHandelDrag=0;
        mrovplVHandelDrag=0;

      });
      obj.mouseleave(function(e){
        mrovplHandelDrag=0;
        mrovplVHandelDrag=0;
        obj.find(".mrovpl-volume-changer").hide();
        obj.find(".mrovpl-btn-volume").removeClass("active");
        obj.find(".morvpl-quality-changer").hide();
        obj.find(".mrovpl-btn-quality").removeClass("active");
        obj.trigger('mouseup');
      });
      obj.mousemove(function(e,px,py){

        if(typeof e.pageY=="undefined"){
          e.pageX=px;
          e.pageY=py;
        }

        if(e.pageY>=handleVDragStart && e.pageY<=handleVDragEnd && mrovplVHandelDrag==1){

          newBottom=e.pageY-handleVDragStart;
          progPercent=((80-newBottom)*100)/(handleVDragEnd-handleVDragStart);
          if(progPercent==0){
            obj.find("#mrovplVolIcon_"+mrovplObjID).attr("class","icon-volume-mute2");
          }else if(progPercent<25){
            obj.find("#mrovplVolIcon_"+mrovplObjID).attr("class","icon-volume-mute");
          }else if(progPercent>=1 && progPercent<30){
            obj.find("#mrovplVolIcon_"+mrovplObjID).attr("class","icon-volume-low");
          }else if(progPercent>=30 && progPercent<60){
            obj.find("#mrovplVolIcon_"+mrovplObjID).attr("class","icon-volume-medium");
          }else{
            obj.find("#mrovplVolIcon_"+mrovplObjID).attr("class","icon-volume-high");
          }
          nVolume=1*(progPercent/100);
          setVolume(nVolume);
          $(this).find('.mrovpl-v-progress').css({
            height:progPercent+"%"
          });

          $(this).find('.mrovpl-v-progress-handel').css({
            bottom:80-newBottom
          })
        }

        if(e.pageX>=handleDragStart && e.pageX<=handleDragEnd && mrovplHandelDrag==1){
          newLeft=e.pageX-handleDragStart;
          progPercent=(newLeft*100)/(handleDragEnd-handleDragStart);
          setTime('p',progPercent)
          $(this).find('.mropl-proggress').css({
            width:progPercent+"%"
          });
          $(this).find('.mropl-proggress-handel').css({
            left:newLeft
          });
        }
      });
    }
    function addEvents_video(obj){
      mrovplMediaPlayer.src=obj.find("#"+activeSrc).attr('src');
      var i = setInterval(function() {
        if(mrovplMediaPlayer.readyState > 0) {
          ttottime=secondsToTime(mrovplMediaPlayer.duration);
          setCurrentTimeString(obj,0);
          obj.find(".mrovpl-total-time").text(ttottime);
          buffInc = $('#mrovplBufferCanvas_'+mrovplObjID).width() / mrovplMediaPlayer.duration;

          clearInterval(i);
        }
      }, 200);
      mrovplMediaPlayer.addEventListener('timeupdate', function(){
        updateProgressBar(obj)
      }, false);
      mrovplMediaPlayer.addEventListener('seeked', function() {

      });
      var ranges = [];
      mrovplMediaPlayer.addEventListener('progress', function()
      {

        for(var i = 0; i < mrovplMediaPlayer.buffered.length; i ++)
        {
          console.log("is:")

          var startX = mrovplMediaPlayer.buffered.start(i) * buffInc;
          var endX = mrovplMediaPlayer.buffered.end(i) * buffInc;
          var width = endX - startX;

          buffContext.fillRect(startX, 0, width, buffCanvas.height);
         //buffContext.rect(startX, 0, width, buffCanvas.height);

        }
        //console.log(ranges)

      }, false);
      mrovplMediaPlayer.onwaiting = function() {
        obj.find(".mrovpl-buffering").addClass("active");
      };
      mrovplMediaPlayer.onplaying = function() {
        obj.find(".mrovpl-buffering").removeClass("active");
      };

    }
    function addEvents_mainProgress(obj){

      obj.find(".mropl-proggress-handel").mousedown(function(e){
        handleDragStart=obj.find(".mrovpl-proggress-bar").offset().left
        handleDragEnd=obj.find(".mrovpl-proggress-bar").offset().left+obj.find(".mrovpl-proggress-bar").width();
        mrovplHandelDrag=1;
      });
      obj.find(".mrovpl-proggress-bar").mouseup(function(e){

        handleDragStart=obj.find(".mrovpl-proggress-bar").offset().left;
        handleDragEnd=obj.find(".mrovpl-proggress-bar").offset().left+obj.find(".mrovpl-proggress-bar").width();
        mrovplHandelDrag=1;
        obj.trigger('mousemove',[e.pageX,e.pageY]);
        mrovplHandelDrag=0;

      });


    }
    function addEvents_volumeProgress(obj){

      obj.find(".mrovpl-v-progress-handel").mousedown(function(e){
        handleVDragStart=obj.find(".mrovpl-v-progress-bar").offset().top;
        handleVDragEnd=obj.find(".mrovpl-v-progress-bar").offset().top+obj.find(".mrovpl-v-progress-bar").height();
        mrovplVHandelDrag=1;
        console.log("a");
      });
      obj.find(".mrovpl-v-progress-bar").mouseup(function(e){
        handleVDragStart=obj.find(".mrovpl-v-progress-bar").offset().top;
        handleVDragEnd=obj.find(".mrovpl-v-progress-bar").offset().top+obj.find(".mrovpl-v-progress-bar").height();
        mrovplVHandelDrag=1;
        obj.trigger('mousemove',[e.pageX,e.pageY]);
        mrovplVHandelDrag=0;

      });



    }
    function addEvents_buttons(obj){
      var obj=obj;
      obj.find(".mrovpl-btn-quality").click(function(){
        obj.find(".morvpl-quality-changer").toggle();
        if(obj.find(".morvpl-quality-changer").css("display")=="none"){
          $(this).removeClass("active");
        }else{
          $(this).addClass("active");
        }
        console.log()
      });
      obj.find(".mrovpl-btn-volume").click(function(){
        obj.find(".mrovpl-volume-changer").toggle();
        if(obj.find(".mrovpl-volume-changer").css("display")=="none"){
          $(this).removeClass("active");
        }else{
          $(this).addClass("active");
        }
      });
      obj.find(".mrovpl-btn-fullscreen").click(function(){
        fullscreen(obj,1);
      });
      obj.find(".mrovpl-btn-refresh").click(function(){
        reply();
      });
      obj.find(".mrovpl-btn-refresh").click(function(){
        reply();
      });
      obj.find(".mrovpl-btn-backward").click(function(){
        backward();
      });
      obj.on('click','button.mrovpl-quality',function(){
        alert("a;i")
        obj.find("button.mrovpl-quality").removeClass("active");
        $(this).addClass('active');
        lab=$(this).attr('data-label');
        obj.find('.mrovpl-btn-quality').text(lab)
        curTime=mrovplMediaPlayer.currentTime;
        source=$(this).attr('data-source');
        activeSrc=source;
        mrovplMediaPlayer.src=obj.find("#"+activeSrc).attr('src');
        mrovplMediaPlayer.currentTime=curTime;
        if(mrovplIsPLaying==1){
          mrovplMediaPlayer.play()

        }else{
          mrovplMediaPlayer.pause()
        }

      })

    };
    function addEvents_playButtons(obj){

      obj.find(".mrovpl-btn-play").click(function(){
        if(mrovplIsPLaying==1){
          mrovplIsPLaying=0;
          obj.find(".mrovpl-btn-play").children('i').attr("class","icon-play");
          obj.find(".mrovpl-buffering").removeClass("active");
          mrovplMediaPlayer.pause()
        }else{
          startPlaying(obj);
          obj.find(".mrovpl-btn-play").children('i').attr("class","icon-pause");
          mrovplIsPLaying=1;
          mrovplMediaPlayer.play()
        }

      });
    };
    function setCurrentTimeString(obj,zero){
      if(zero==0){
        secs=0;
      }else{
        secs=mrovplMediaPlayer.currentTime;
      }
      tsecs=mrovplMediaPlayer.duration;
      if(tsecs>3600){
        fh=1;
      }else{
        fh=0;
      }
      curTimeString=secondsToTime(secs,fh)
      obj.find(".mrovpl-current-time").text(curTimeString);
    }
    function secondsToTime(secs,fh){
      var sec_num = parseInt(secs, 10); // don't forget the second param
      var hours   = Math.floor(sec_num / 3600);
      var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
      var seconds = sec_num - (hours * 3600) - (minutes * 60);

      if (hours   < 10) {hours   = "0"+hours;}
      if (minutes < 10) {minutes = "0"+minutes;}
      if (seconds < 10) {seconds = "0"+seconds;}
      output="";
      if(hours!="00" || fh==1){
        output=hours+':';
      }
      output=minutes+':'+seconds;

      return output;
    }
    /*------------------------------------------------------------------------*/
    /* Player Control Functions                                               */
    /*------------------------------------------------------------------------*/
    function setVolume(vol){
      mrovplMediaPlayer.volume=vol;
    }
    function fullscreen(obj){
      if(obj.hasClass('mrovpl-fullscreen')){
        obj.removeClass("mrovpl-fullscreen");
        obj.find('.mrovpl-btn-fullscreen').children('i').attr("class","icon-enlarge");
      }else{
        obj.addClass("mrovpl-fullscreen");
        obj.find('.mrovpl-btn-fullscreen').children('i').attr("class","icon-shrink");
      }

      //$('body').append(obj);
    }
    function reply(){
      mrovplMediaPlayer.currentTime=0;
      if(mrovplIsPLaying==1){
        mrovplMediaPlayer.play()

      }else{
        mrovplMediaPlayer.pause()
      }
    }
    function setTime(mode,value){
      if(mode=="p"){
        mvD=mrovplMediaPlayer.duration;
        newCurTime=((value)*mvD)/100
        mrovplMediaPlayer.currentTime=newCurTime;
      }else{
        mrovplMediaPlayer.currentTime=value;
      }
    }
    function updateProgressBar(obj){
      nPercent=(mrovplMediaPlayer.currentTime*100)/mrovplMediaPlayer.duration;


      obj.find(".mropl-proggress").css({
        width:nPercent+"%"
      })
      obj.find(".mropl-proggress-handel").css({
        left:nPercent+"%"
      })
      setCurrentTimeString(obj,mrovplMediaPlayer.currentTime);
    }
    function backward(){
      //console.log(mrovplMediaPlayer.currentTime,mrovplMediaPlayer.duration)
      try {


        mrovplMediaPlayer.currentTime -= 10;


      } catch (err) {
        // errMessage(err) // show exception
        console.log("Video content might not be loaded");
      }
    }
    function startPlaying(obj){
      if(!obj.hasClass('mrovpl-start'))
        obj.addClass('mrovpl-start')
    }
  };
  $.fn.mrplayer.defaults = {
    width: "100%",
    height: "340px",
    image:""
  };


}( jQuery ));
